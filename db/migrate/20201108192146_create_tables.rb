class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :login
      t.string :password
      t.string :token
      t.bigint :user_room_id
      t.timestamps
    end

    create_table :rooms do |t|
      t.string :name
      t.string :last_step_token
      t.json :game_place
      t.timestamps
    end

    create_table :user_rooms do |t|
      t.string :state
      t.string :color
      t.timestamps
    end

  end
end
