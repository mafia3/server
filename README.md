# README

 Для инициализации 

* docker-compose build

* docker-compose run --rm web rake db:create db:migrate

 Для обновления базы

* docker-compose run --rm web rake db:drop db:create db:migrate

 Для запуска после инициализации

* docker-compose up -d

* Настроить файл .env

 Для просмотра логов

* tail -f log/development.log

