class ApplicationError < StandardError
    attr_reader :message, :code
    def initialize(message="Ошибка приложения",code=500)
      @message = message
      @code = code
      super(message)
    end 
  end
