class GenerateBoard
attr_reader :i, :color
  def initialize
  end
  def call
    generate(1, color: 'black') +
    generate(2, parity: false, color: 'black') +
    generate(3, color: 'black') +
    generate(4) +
    generate(5) +
    generate(6, parity: false, color: 'white') +
    generate(7, color: 'white') +
    generate(8, parity: false, color: 'white')
  end

  private

  def generate(i_, parity: true, color: nil)
    ids_shashki = (1..8).to_a.map {|id| id  if id%2 == 0 && parity || id%2 == 1 && !parity}.compact

    (1..8).to_a.map do |id|
      {i: i_, j:id, color: ids_shashki.include?(id) ? color : nil}
    end
  end
end