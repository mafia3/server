# frozen_string_literal: true
class CheckSession
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :check_session

  def perform
    return if ENV['SKIP_SIDEKIQ'] == 'true'
    users = User.where.not(token: nil).where('updated_at < ?', Time.at(Time.now.to_i - 5))
    users.each do |user|
      UserRoom.where(user_id: user.id).destroy_all
      user.update!(token: nil)
    end
  end
end