class User < ApplicationRecord
  validates :login, presence: {message: "Логин должен присутствовать"}, uniqueness: {message: "Такой логин уже сущетвует"}
  validates :password, presence: {message: "Пароль должен присутствовать"}
end
