class UserRoom < ApplicationRecord
  has_many :users
  has_many :rooms

  validates :user_id, presence: { message: "Пользователь должен присутствовать" }, uniqueness: {message: "Пользователь уже зашел в комнату" }
  validates :room_id, presence: { message: "Комната должна присутствовать" }
  validate :check_count_users_in_room, on: :create

  private
  
  def check_count_users_in_room
    ApplicationError.new('В комнате максмум 2 игрока', 422) if UserRoom.where(room_id: self.room_id).count >= 2
  end

end
