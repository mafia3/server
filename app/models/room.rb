class Room < ApplicationRecord
  validates :name, presence: {message: "Имя должно присутствовать"}, uniqueness: { message: "Такое имя уже существует"}

end
