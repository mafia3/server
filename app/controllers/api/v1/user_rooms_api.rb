module API
    module V1
      class UserRoomsApi < API::V1::Base
        format :json
  
        resource :user_rooms do
          before do
            raise ApplicationError.new('Вы не авторизованы', 403) if User.find_by_token(request.headers['Token']).blank? unless ENV['SKIP_AUTH']=='true'
            
          end

          helpers do
            def declaring_params
              declared(params, include_missing: false)
            end

            def current_user
              @user ||= User.find_by_token(request.headers['Token'])
            end
          end

          desc 'Войти в комнату', success: Entities::UserRoom
          params do
            requires :name , type: String, desc: 'Имя комнаты'
          end
  
          post do
            room = Room.find_by_name!(declaring_params[:name])
            current_room = UserRoom.where(room_id: room.id)
            raise ApplicationError.new('Комната заполнена', 422) if current_room.count >= 2
            user_room = UserRoom.create!(room_id: room.id, user_id: current_user.id, state: 'wait')
            if UserRoom.where(room_id: room.id).count == 2
              user_room.update!(color: 'black')
              room.update(last_step_token: current_user.token)
            else
              user_room.update!(color: 'white')
            end
             present user_room, with: API::Entities::UserRoom
          end
  
          desc 'Получить свою сессию', success: Entities::UserRoom
          get do
            room_id = UserRoom.where(user_id: current_user.id).pluck(:room_id)
            present UserRoom.where(room_id: room_id), with: API::Entities::UserRoom
          end

          desc 'Изменить состояние', success: Entities::UserRoom
          params do
            requires :state , type: String, desc: 'Состояние', values: ['wait','ready','in_game']
          end 
          patch do
            raise ApplicationError.new('У вас нет соперника', 422) if UserRoom.where(room_id: UserRoom.where(user_id: current_user.id).pluck(:room_id).first).count < 2
            user_room = UserRoom.find_by(user_id: current_user.id)
            user_room.update!(state: declaring_params[:state])
            present user_room.reload, with: API::Entities::UserRoom
          end

          desc 'Выйти из комнаты', success: Entities::UserRoom
          delete do
            room_ids = UserRoom.where(user_id: current_user.id).pluck(:room_id).uniq
            UserRoom.where(user_id: current_user.id).destroy_all
            UserRoom.where(room_id: room_ids).pluck(:id).each { |id| UserRoom.find(id).update!(state: 'wait')}
            Room.where(id: room_ids).each { |room| room.update!(game_place: DEFAULT_BOARD) }
            status :no_content
          end
          
        end
      end
    end
  end