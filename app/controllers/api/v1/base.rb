module API
  module V1
    class Base < Grape::API
      version 'v1', using: :path
      format :json

      DEFAULT_BOARD = {white: 0, black:0, board: GenerateBoard.new.call}.freeze
      
      helpers do
        def current_user
          @user ||= User.find_by_token(request.headers['Token'])
        end
      end
      rescue_from ApplicationError do |e|
        error!({ error: e.message }, e.code)
      end

      rescue_from StandardError do |e|
        error!({ error: e.message }, 500)
      end

      mount API::V1::UsersApi
      mount API::V1::RoomsApi
      mount API::V1::UserRoomsApi
    end
  end
end
