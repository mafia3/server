module API
    module V1
      class RoomsApi < API::V1::Base
        format :json
  
        resource :rooms do
          before do
            raise ApplicationError.new('Вы не авторизованы', 403) if User.find_by_token(request.headers['Token']).blank? unless ENV['SKIP_AUTH']=='true'
          end

          helpers do
            def declaring_params
              declared(params, include_missing: false)
            end
          end
  
          desc 'Получить список комнат', success: Entities::Room
          get do
            present Room.all, with: API::Entities::Room
          end

          route_param :name, type: String do
            desc 'Создать комнату', success: Entities::Room
  
            post do
              begin
                room = Room.create!(name: params[:name], game_place: DEFAULT_BOARD)
              rescue
                raise ApplicationError.new('Имя комнаты уже существует', 422)
              end
              present room, with: API::Entities::Room
            end

            desc 'Получить комнату', success: Entities::Room
  
            get do
              begin
                room = Room.find_by_name!(params[:name])
              rescue
                raise ApplicationError.new('Комнаты с таким именем не существует', 422)
              end
              present room, with: API::Entities::Room, show: true
            end

            desc 'Удалить комнату', success: Entities::Room
            delete do
              Room.find_by_name!(params[:name]).destroy!
              
              status :no_content
            end

            resource :step do
              before do
                raise ApplicationError.new('Сейчас не ваш ход', 422) if Room.find_by_name!(params[:name]).last_step_token == request.headers['Token']
              end

              helpers do
                def current_room
                  @current_room ||= Room.find_by_name!(params[:name])
                end
              end

              desc 'Сходить', success: Entities::Room
              params do
                requires :game_place , type: JSON, desc: 'Игровая зона'
              end 
              patch do
                # TODO Возможно реализовать обработку ходов на сервере
                # Step.new.call
                raise ApplicationError.new('Сейчас не ваш ход', 422) if current_room.last_step_token == current_user.token
                current_room.update!(last_step_token: request.headers['Token'], game_place: params[:game_place])

                present current_room.reload, with: API::Entities::Room
              end
            end
          end
          
        end
      end
    end
  end
  