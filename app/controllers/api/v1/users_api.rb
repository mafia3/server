module API
    module V1
      class UsersApi < API::V1::Base
        format :json
  
        resource :users do
  
          helpers do
            def declaring_params
              declared(params, include_missing: false)
            end
          end

          desc 'Зарегестрировать пользователя', success: Entities::User
          params do
            requires :all, using: Entities::User.for_create(except: %i[id updated_at created_at token])
          end
  
          post do
            begin
              user = User.create!(declaring_params)
            rescue
              raise ApplicationError.new('Логин уже существует', 422)
            end
            present user, with: API::Entities::User
          end
  
          desc 'Аутентификация, получение токена', success: Entities::User
          params do
            requires :all, using: Entities::User.for_filter(except: %i[id name updated_at created_at token])
          end
  
          get do
            raise ApplicationError.new('Неправильный логин или пароль', 403) unless User.exists?(login: declaring_params[:login], password: declaring_params[:password])
            user = User.find_by!(login: declaring_params[:login], password: declaring_params[:password])
            UserRoom.where(user_id: user.id).destroy_all
            user.update!(token: SecureRandom.uuid)
            present user.reload, with: API::Entities::User
          end

          resource :ping do
            desc 'Проверка соединения (изменение updated_at)', success: Entities::User
            post do
              begin
                user = User.find_by_token!(request.headers['Token'])
                user.update!(updated_at:Time.now)
              rescue
                raise ApplicationError.new('Вы не авторизованы', 403)
              end
              present user.reload, with: API::Entities::User
            end
          end
          
        end
      end
    end
  end
  