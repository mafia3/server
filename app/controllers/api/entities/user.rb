# frozen_string_literal: true
module API
    module Entities
      class User < API::Entities::Base
        expose :created_at, documentation: { type: DateTime, desc: 'Дата создания записи', required: true }
        expose :updated_at, documentation: { type: DateTime, desc: 'Дата обновления записи', required: true }
        expose :id, documentation: { type: Integer, desc: 'Идентификатор', required: true }
        expose :name, documentation: { type: String, desc: 'Имя', required: true }
        expose :login, documentation: { type: String, desc: 'Логин', required: true }
        expose :password, if: {show: true}, documentation: { type: String, desc: 'Пароль', required: true }
        expose :token, documentation: { type: String, desc: 'Токен', required: true }
      end
    end
  end
  