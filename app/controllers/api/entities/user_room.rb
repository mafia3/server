# frozen_string_literal: true
module API
    module Entities
      class UserRoom < API::Entities::Base
        expose :id, documentation: { type: Integer, desc: 'Идентификатор', required: true }
        expose :user_name, documentation: { type: String, desc: 'Имя пользователя', required: true } do |user_room|
          ::User.find(user_room.user_id).name
        end
        expose :name_room, documentation: { type: String, desc: 'Имя комнаты', required: true } do |user_room|
          ::Room.find(user_room.room_id).name
        end
        expose :color, documentation: { type: String, desc: 'Имя комнаты', required: true } do |user_room|
          user_room.color
        end
        expose :state, documentation: { type: String, desc: 'Статус игрока', required: true } 
      end
    end
  end
  