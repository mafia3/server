# frozen_string_literal: true
module API
    module Entities
      class Room < API::Entities::Base
        expose :id, documentation: { type: Integer, desc: 'Идентификатор', required: true }
        expose :name, documentation: { type: String, desc: 'Имя комнаты', required: true }
        expose :count_users, documentation: { type: String, desc: 'Количество игроков', required: true } do |room|
          ::UserRoom.where(room_id: room.id).count
        end
        expose :game_place, if: {show: true}, documentation: { type: JSON, desc: 'Игровое поле', required: true }
        #отправлять количество пользоветелей в комнате
      end
    end
  end
  